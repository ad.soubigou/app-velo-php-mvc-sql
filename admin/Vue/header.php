<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter mon Vélo - AppVelo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:ital,wght@0,100..700;1,100..700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body>
    <header class="container">
        <nav class="navbar navbar-expand-lg bg-body-tertiary">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php">Appli Vélo</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                    <ul class="navbar-nav d-flex justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?action=velos">Nos Vélos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php?action=voles">Vélos Volés</a>
                        </li>
                        <?php if (isset($_SESSION['login'])) { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?action=ajout">Ajouter un vélo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="deconnexion.php">
                                    Déconnexion
                                </a>
                            <?php } else { ?>
                                <a class="nav-link " href="index.php?action=login">
                                    Connexion / Inscription
                                </a>
                            <?php    } ?>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>