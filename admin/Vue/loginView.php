<main class="container">
    <form action="./login.php" class="col-6 mx-auto pt-5" method="post">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Login</label>
            <input type="text" class="form-control" name="login" required>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Envoyer</button>
        <div class="form-text">Pas encore de compte ? Cliquez <a href="index.php?action=signup">ici</a>.</div>
    </form>
</main>