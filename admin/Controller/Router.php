<?php

require_once './admin/Model/Velo.php';
require_once './admin/Model/Users.php';
require_once './admin/Model/Images.php';

try {
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'voles') {
            $vole = new Velo();
            $velosVoles = $vole->getAllVelosVoles();
            require_once './admin/Vue/volesView.php';
        } else if ($_GET['action'] == 'contact') {
            if (isset($_GET['userID'])) {
                $id = $_GET['userID'];
            }
            require_once './admin/Vue/contactView.php';
        } else if ($_GET['action'] == 'login') {
            require_once './admin/Vue/loginView.php';
        } else if ($_GET['action'] == 'signup') {
            require_once './admin/Vue/signupView.php';
        } else if ($_GET['action'] == 'ajout') {
            require_once './admin/Vue/ajoutVelo.php';
        } else if ($_GET['action'] == 'addVelo') {
            if (isset($_POST['new_model'])) {
                $userId = $_GET['userID'];
                $modele = htmlspecialchars($_POST['new_model']);
                $dossier = './assets/img/';
                $extensions_autorisees = array('png', 'jpeg', 'jpg', 'webp');
                $photo_name = array();
                if (isset($_FILES['image'])) {
                    foreach ($_FILES['image']['tmp_name'] as $key => $tmp_name) {
                        if (!empty($_FILES['image']['name'][0])) {
                            $nom_photo = $_FILES['image']['name'][$key];
                            $temp_name = $_FILES['image']['tmp_name'][$key];
                            $infos_fichier = pathinfo($nom_photo);
                            $extension_upload = strtolower($infos_fichier['extension']);
                            if (!is_uploaded_file($temp_name)) {
                                exit("Le fichier est introuvable");
                            }
                            if ($_FILES['image']['size'][$key] > 1000000) {
                                exit("Erreur : fichier trop volumineux");
                            }
                            if (!in_array($extension_upload, $extensions_autorisees)) {
                                exit("Erreur : le format du fichier n'est pas accpeté. Formats acceptés : jpg, jpeg, png, webp");
                            }
                            $photo_existe = $dossier . $nom_photo;
                            if (file_exists($photo_existe)) {
                                exit("Nom de fichier existant, veuillez le renommer.");
                            }
                            if (!move_uploaded_file($temp_name, $dossier . $nom_photo)) {
                                exit("Erreur lors de l'upload du fichier");
                            }
                            $photo_name[] = $nom_photo;
                        } else {
                            $photo_name[] = "velo_app_img.webp";
                        }
                    }
                }
                $new_velo = new Velo();
                $_POST['velo_vole'] == 'oui' ? $vole = 1 : $vole = 0;
                $new_velo->addVelo($modele, $vole, $userId, $photo_name);
                header('Location: index.php');
            }
        } else if ($_GET['action'] == 'details') {
            if (isset($_GET['veloID'])) {
                $veloId = $_GET['veloID'];
                $velo_detail = new Velo();
                $details = $velo_detail->getVeloAndOwner($veloId);
                $image = new Images();
                $path_img = $image->getImagePath($veloId);
                require_once './admin/Vue/detailsVelo.php';
            }
        } else if ($_GET['action'] == 'velos') {
            $velo = new Velo();
            $velos = $velo->getAllVelos();
            require_once './admin/Vue/velosView.php';
        } else if ($_GET['action'] == 'declaration') {
            $veloId = $_GET['id'];
            $velo = new Velo();
            $velo->declarerVeloVole($veloId);
            header('Location: index.php');
        } else if ($_GET['action'] == 'delete') {
            if (isset($_GET['id'])) {
                $veloID = $_GET['id'];
                $image = new Images();
                $paths = $image->getAllImagesPaths($veloID);
                $dossier = './assets/img/';
                foreach ($paths as $path) {
                    $file_name = $dossier . $path['path'];
                    if (file_exists($file_name)) {
                        unlink($file_name);
                    }
                }
                $velo = new Velo();
                $velo->supprimerVelo($veloID);
                header('Location: index.php');
            }
        }
    } else {
        $velo = new Velo();
        $velos = $velo->getVelo();
        $velosVoles = $velo->getVeloVole();
        require_once './admin/Vue/accueilView.php';
    }
    if (isset($_GET['status'])) {
        if ($_GET['status'] == 'logged') { ?>
            <script>
                alert("Bienvenue <?= $_SESSION['login'] ?>.")
            </script> <?php
                    } else if ($_GET['status'] == 'sent') {
                        ?>
            <script>
                alert("Message envoyé.")
            </script> <?php
                    }
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $file = basename($e->getFile());
                $line = $e->getLine();

                require_once './admin/Vue/errorView.php';
            }
