<?php

abstract class Model
{
    protected  function connect()
    {
        // Connexion à la BdD
        $db = new PDO('mysql:host=localhost; dbname=velo; charset=utf8', 'root', '');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    }
}
