    <main class="container">
        <div class="py-5">
            <div class="d-flex justify-content-between align-items-center">
                <h1>Nos vélos</h1>
                <input type="search" placeholder="Recherche..." id="searchInput">
            </div>
            <div class="container d-flex flex-wrap justify-content-evenly row-gap-4">
                <?php foreach ($velos as $velo) { ?>
                    <div class="card" style="width: 18rem;">
                        <img src="./assets/img/<?= $velo['path'] ?>" class="card-img-top" alt="photo <?= $velo['modele'] ?><">
                        <div class="card-body d-flex flex-column justify-content-between">
                            <h5 class="card-title"><?= $velo['modele'] ?></h5>
                        </div>
                        <a href="index.php?action=details&veloID=<?= $velo['id'] ?>"><button class="btn">Afficher Détails</button></a>
                    </div><?php } ?>
            </div>
        </div>
    </main>
    <script src="./assets/js/search.js" defer></script>