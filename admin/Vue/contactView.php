    <main class="container">
        <div class="pt-5 col-8 mx-auto">
            <h1>Formulaire de contact</h1>
            <form action="./contact.php" method="post" class="pt-3">
                <input type="hidden" name="id" value="<?= $id ?>">
                <div class="mb-3">
                    <label for="pseudo" class="form-label">Pseudo</label>
                    <input type="text" name="pseudo" class="form-control" placeholder="Votre pseudo">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="name@example.com">
                </div>
                <div class="mb-3">
                    <label for="message" class="form-label">Votre message</label>
                    <textarea class="form-control" name="message" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary mb-3">Envoyer</button>
            </form>
        </div>
    </main>