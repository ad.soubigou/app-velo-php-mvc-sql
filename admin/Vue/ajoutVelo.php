    <main class="container">
        <form action="index.php?action=addVelo&userID=<?= $_SESSION['id'] ?>" class=" col-10 col-lg-6 mx-auto pt-5" method="post" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="modele" class="form-label">Modèle du vélo</label>
                <input type="text" class="form-control" name="new_model" required>
            </div>
            <div class="d-flex justify-content-start ">
                <p class="me-2">Votre vélo a-t-il été volé ?</p>
                <div class="form-check mx-2">
                    <input class="form-check-input" type="radio" name="velo_vole" value="oui">
                    <label class="form-check-label" for="flexCheckDefault">
                        Oui
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="velo_vole" value="non">
                    <label class="form-check-label" for="flexCheckChecked">
                        Non
                    </label>
                </div>
            </div>
            <div class="mb-3">
                <label for="image" class="form-label">Choisissez une image à uploader (jpeg, jpg, png, webp): </label>
                <input type="file" class="form-control " name="image[]" multiple>
            </div>
            <button type="submit" class="btn btn-primary">Envoyer</button>
        </form>

    </main>