-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 04, 2024 at 06:50 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `velo`
--

-- --------------------------------------------------------

--
-- Table structure for table `image_velo`
--

CREATE TABLE `image_velo` (
  `id` int NOT NULL,
  `velo_id` int NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_velo`
--

INSERT INTO `image_velo` (`id`, `velo_id`, `path`) VALUES
(10, 30, 'btwin-orignal-500.jpg'),
(11, 31, 'decathlon-bacirctwin-original-700.jpg'),
(12, 32, 'vanrysel-rcr-sram-force.jpg'),
(13, 33, 'velo_app_img.webp'),
(14, 34, 'MELBOURNE-Evobike.png'),
(23, 42, 'genesis-croix-de-fer-10.jpg'),
(24, 42, 'pulse-elite-t52.webp'),
(25, 43, 'velo_app_img.webp');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `login` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `email`) VALUES
(1, 'adrien', 'motdepasse', 'ad.soubigou@gmail.com'),
(2, 'anthony', 'motdepasse', 'ad.soubigou@gmail.com'),
(3, 'Clément', 'Jullie', 'clementjullie@gmail.com'),
(4, 'adrien21', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'ad.soubigou@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `velo`
--

CREATE TABLE `velo` (
  `id` int NOT NULL,
  `modele` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `vole` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `velo`
--

INSERT INTO `velo` (`id`, `modele`, `user_id`, `vole`) VALUES
(30, 'B&#039;Twin Original 500', 1, 1),
(31, 'B&#039;Twin Original 700', 1, 0),
(32, 'Van Rysel RCR Sram Force eTap AXS', 1, 1),
(33, 'Mondraker Chrono Carbon SE Sram GX Eagle', 1, 0),
(34, 'Evobike Melbourne', 2, 1),
(42, 'Van Rysel RCR Sram Force eTap AXS', 3, 1),
(43, 'MEGAMO PULSE ELITE CARBONE AXS', 3, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image_velo`
--
ALTER TABLE `image_velo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `velo`
--
ALTER TABLE `velo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image_velo`
--
ALTER TABLE `image_velo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `velo`
--
ALTER TABLE `velo`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `velo`
--
ALTER TABLE `velo`
  ADD CONSTRAINT `velo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
