<?php

require_once './admin/Model/Users.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupération des données du formulaire
    $id = $_POST['id'];
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $email = htmlspecialchars($_POST['email']);
    $message = htmlspecialchars($_POST['message']);

    $user = new Users();
    $destinataire = $user->getUserMail($id);


    // Mise en forme de l'email à envoyer
    $to = $destinataire->fetch()['email'];
    $subject = "Nouveau message de $pseudo";
    $body = "Nouveau message depuis le site Appli Vélo : \n\n Expéditeur : $pseudo ($email):\n\n$message";
    $headers = "From: $email";

    // Envoyer l'email
    if (mail($to, $subject, $body, $headers)) { ?>
        <script>
            window.location.href = "index.php?status=sent";
        </script>
<?php
    } else {
        echo "Échec de l'envoi du message.";
    }
}
