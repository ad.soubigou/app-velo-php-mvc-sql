<?php

require_once './admin/Model/Users.php';

session_start();
if (isset($_POST)) {
    if (!empty($_POST['login']) && !empty($_POST['password']) && !empty($_POST['email'])) {
        $login = htmlspecialchars($_POST['login']);
        $email = htmlspecialchars($_POST['email']);
        $password = sha1(htmlspecialchars($_POST['password']));
        $user = new Users();
        $user_exist = $user->userExists($login, $email);
        $register_ok = false;
        foreach ($user_exist as $exist) {
            if ($exist['login'] == $login || $exist['email'] == $email) { ?>
                <script>
                    alert("Login ou email déjà existant !");
                    window.location.href = "index.php?action=signup";
                </script>
<?php break;
            } else {
                $register_ok = true;
            }
        }
        if ($register_ok == true) {
            $register = $user->userRegister($login, $password, $email);
            $connexion = $user->userConnect($login, $password);
            if ($connexion->rowCount() > 0) {
                $_SESSION['login'] = $login;
                $_SESSION['password'] = $password;
                $_SESSION['id'] = $connexion->fetch()['id'];
                header('Location: index.php?status=logged');
            }
        }
    }
}
