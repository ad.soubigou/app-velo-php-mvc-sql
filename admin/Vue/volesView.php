<main class="container pt-5">
    <div class="d-flex justify-content-between align-items-center">
        <h1>Nos vélos</h1>
        <input type="search" placeholder="Recherche..." id="searchInput">
    </div>
    <div class="container d-flex flex-wrap justify-content-evenly row-gap-4 pt-3">
        <?php foreach ($velosVoles as $veloVole) { ?>
            <div class="card" style="width: 18rem;">
                <img src="./assets/img/<?= $veloVole['path'] ?>" class="card-img-top" alt="photo <?= $veloVole['modele'] ?>">
                <div class="card-body d-flex flex-column justify-content-between">
                    <h5 class="card-title"><?= $veloVole['modele'] ?></h5>
                    <?php if (isset($veloVole['user_id'])) { ?>
                        <a href="<?= "index.php?action=contact&userID=" . $veloVole['user_id'] ?>" class="btn btn-primary">Contacter le propriétaire</a>
                    <?php  } ?>
                </div>
            </div> <?php } ?>
    </div>
</main>
<script src="./assets/js/search.js" defer></script>