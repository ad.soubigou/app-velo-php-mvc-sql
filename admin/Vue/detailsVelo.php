    <main class="container">
        <div class="py-5">
            <div class="container d-flex flex-wrap justify-content-evenly row-gap-4">

                <?php foreach ($details as $detail) { ?>
                    <div class="card" style="width: 18rem;">
                        <img src="./assets/img/<?= $path_img[0]['path'] ?>" class="card-img-top" alt="photo <?= $details[0]['modele'] ?>">
                        <div class="card-body">
                            <h5 class="card-title"><?= $details[0]['modele'] ?></h5>
                            <p>Propriétaire : <?= $details[0]['login'] ?></p>
                            <?php if ($details[0]['vole'] == 1) { ?>
                                <p>Status : Volé</p>
                            <?php } else if ($details[0]['vole'] == 0 && isset($_SESSION['id'])) { ?>
                                <form action="index.php?action=declaration&id=<?= $details[0]['id'] ?>" method="post">
                                    <button class="btn btn-danger">Déclarer un vol</button>
                                </form>
                            <?php  } else { ?>
                                <p>Status : Non volé</p>
                            <?php }
                            if (isset($_SESSION['id'])) { ?>
                                <form action="index.php?action=delete&id=<?= $details[0]['id'] ?>" method="post">
                                    <button type="submit" class="btn btn-warning mt-2">Supprimer</button>
                                </form>
                            <?php   }
                            ?>

                        </div>
                    </div><?php } ?>
            </div>
        </div>
    </main>