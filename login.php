<?php
require_once './admin/Model/Users.php';

// Démarre une nouvelle session ou reprend une session existante
session_start();
if (isset($_POST)) {
    if (!empty($_POST['login']) && !empty($_POST['password'])) {
        $login = htmlspecialchars($_POST['login']);
        $password = sha1(htmlspecialchars($_POST['password']));
        $user = new Users();
        // Fonction permettant de connecter un utilisateur
        $connexion = $user->userConnect($login, $password);
        // Si la fonction renvoie un tableau non vide
        if ($connexion->rowCount() > 0) {
            // Super variable $_SESSION récupère les infos de connexion
            $_SESSION['login'] = $login;
            $_SESSION['password'] = $password;
            $_SESSION['id'] = $connexion->fetch()['id'];
            header('Location: index.php?status=logged');
        } else {
?>
            <script>
                alert("Login ou mdp incorrect");
                window.location.href = "index.php?action=login";
            </script>";
<?php
        }
    }
}
