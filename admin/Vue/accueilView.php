    <main class="container">
        <div class="py-5">
            <h1>Nos derniers vélos</h1>
            <div class="container d-flex flex-wrap justify-content-evenly row-gap-4">
                <?php foreach ($velos as $velo) { ?>
                    <div class="card" style="width: 18rem;">
                        <img src="./assets/img/<?= $velo['path'] ?>" class="card-img-top" alt="photo <?= $velo['modele'] ?>">
                        <div class="card-body d-flex flex-column justify-content-between">
                            <h5 class="card-title"><?= $velo['modele'] ?></h5>
                        </div>
                        <a href="index.php?action=details&veloID=<?= $velo['id'] ?>"><button class="btn">Afficher Détails</button></a>
                    </div><?php } ?>
            </div>
        </div>
        <div class="pb-5">
            <h2>Derniers vélos déclarés volés</h2>
            <div class="container d-flex flex-wrap justify-content-evenly row-gap-4">
                <?php foreach ($velosVoles as $veloVole) { ?>
                    <div class="card" style="width: 18rem;">
                        <img src="./assets/img/<?= $veloVole['path'] ?>" class="card-img-top" alt="photo <?= $veloVole['modele'] ?>">
                        <div class="card-body d-flex flex-column justify-content-between">
                            <h5 class="card-title"><?= $veloVole['modele'] ?></h5>
                        </div>
                        <a href="index.php?action=details&veloID=<?= $veloVole['id'] ?>"><button class="btn">Afficher Détails</button></a>
                    </div> <?php } ?>
            </div>
        </div>
    </main>