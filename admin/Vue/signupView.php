<main class="container">
    <form class="col-6 mx-auto pt-5" method="post" action="./register.php">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Votre adresse mail</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
        </div>
        <div class="mb-3">
            <label for="login" class="form-label">Login</label>
            <input type="text" class="form-control" name="login" required>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>
</main>