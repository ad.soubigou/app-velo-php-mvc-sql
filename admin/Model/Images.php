<?php

require_once './admin/Model/Model.php';

class Images extends Model
{
    // Fonction permettant de récupérer le chemin d'une image selon l'id du vélo
    public function getImagePath($id)
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT path FROM `image_velo` WHERE velo_id = ? LIMIT 1');
        $req->execute([$id]);
        return $req->fetchAll();
        $db = null;
    }

    public function getAllImagesPaths($id)
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT path FROM `image_velo` WHERE velo_id = ?');
        $req->execute([$id]);
        return $req->fetchAll();
        $db = null;
    }
}
