<?php

require_once 'Model.php';

class Velo extends Model
{
    public function getVelo()
    {
        $db = $this->connect();
        // Utilisation de la fonction SQL MIN() pour ne sélectionner qu'une image dans la base lorsque le vélo est associé à plusieurs. MIN() renvoie le premier "path" par ordre alphabétique.
        $req = $db->prepare('SELECT id, modele, user_id, vole, (
            SELECT MIN(path)
            FROM image_velo
            WHERE velo_id = velo.id
        ) AS path
        FROM velo
        ORDER BY velo.id DESC
        LIMIT 3;');
        $req->execute();
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant de récupérer tous les vélos
    public function getAllVelos()
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT id, modele, user_id, vole, (
            SELECT MIN(path)
            FROM image_velo
            WHERE velo_id = velo.id
        ) AS path
        FROM velo ORDER BY velo.modele');
        $req->execute();
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant de récupérer les 3 derniers vélos volés
    public function getVeloVole()
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT id, modele, user_id, vole, (
            SELECT MIN(path)
            FROM image_velo
            WHERE velo_id = velo.id
        ) AS path
        FROM velo WHERE velo.vole = 1 ORDER BY velo.id DESC LIMIT 3');
        $req->execute();
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant de récupérer tous les vélos volés
    public function getAllVelosVoles()
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT id, modele, user_id, vole, (
            SELECT MIN(path)
            FROM image_velo
            WHERE velo_id = velo.id
        ) AS path
        FROM velo WHERE vole = 1 ORDER BY velo.id DESC');
        $req->execute();
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant de récupérer un vélo et le nom de son propriétaire
    public function getVeloAndOwner($veloId)
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT velo.modele, velo.vole, velo.id, `login` FROM `users` JOIN velo ON users.id = velo.user_id WHERE velo.id = ? ');
        $req->execute([$veloId]);
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant d'ajouter un vélo
    public function addVelo($modele, $vole, $userId, $img)
    {
        $db = $this->connect();
        $req = $db->prepare('INSERT INTO `velo`(`modele`, `vole`, `user_id`) VALUES (:modele, :vole, :userId)');
        $req->bindValue(':modele', $modele);
        $req->bindValue(':vole', $vole);
        $req->bindValue(':userId', $userId);
        $req->execute();
        $id = $db->lastInsertId();
        $req_img = $db->prepare('INSERT INTO `image_velo`(`velo_id`, `path`) VALUES (:veloId, :img)');
        // Boucle permettant d'ajouter plusieurs images à un même vélo
        foreach ($img as $image) {
            $req_img->bindValue(':veloId', $id);
            $req_img->bindValue(':img', $image);
            $req_img->execute();
        }
        $db = null;
    }

    // Fonction permettant de déclarer le vol d'un vélo
    public function declarerVeloVole($id)
    {
        $db = $this->connect();
        $req = $db->prepare('UPDATE `velo` SET `vole`= 1 WHERE `id` = ?');
        $req->execute([$id]);
        $db = null;
    }

    // Fonction permettant de supprimer un vélo
    public function supprimerVelo($id)
    {
        $db = $this->connect();
        $req_img = $db->prepare('DELETE FROM `image_velo` WHERE `velo_id` = ?');
        $req_img->execute([$id]);
        $req = $db->prepare('DELETE FROM `velo` WHERE `id` = ?');
        $req->execute([$id]);
        $db = null;
    }
}
