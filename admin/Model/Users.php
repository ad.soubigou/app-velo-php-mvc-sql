<?php
require_once 'Model.php';

class Users extends Model
{
    // Fonction permettant de récupérer le mail de l'utilisateur selon son id
    public function getUserMail($id)
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT `email` FROM `users` WHERE id = ? ');
        $req->execute([$id]);
        return $req;
        $db = null;
    }

    // Fonction permettant de gérer la connexion de l'utilisateur
    public function userConnect($login, $password)
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT * FROM users WHERE login = ? AND password = ?');
        $req->execute(array($login, $password));
        return $req;
        $db = null;
    }

    public function userExists()
    {
        $db = $this->connect();
        $req = $db->prepare('SELECT * FROM users');
        $req->execute();
        return $req->fetchAll();
        $db = null;
    }

    // Fonction permettant de gérer l'enregistrement de l'utilisateur
    public function userRegister($login, $password, $email)
    {
        $db = $this->connect();
        $req = $db->prepare('INSERT INTO users(login, password, email) VALUES (?, ?, ?)');
        $req->execute(array($login, $password, $email));
        return $req;
        $db = null;
    }
}
